//
//  DrawView.h
//  VinoTwo
//
//  Created by Ethan on 2014/11/12.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BufferManager.h"

@interface DrawView : NSOpenGLView{
    GLfloat * _leftBuffer;
    GLfloat * _rightBuffer;
}

@property (nonatomic, strong) NSTimer* drawTimer;
@property (nonatomic, weak) BufferManager * mgr;

@end
