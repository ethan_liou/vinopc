//
//  DrawView.m
//  VinoTwo
//
//  Created by Ethan on 2014/11/12.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import "DrawView.h"
#import <OpenGL/gl.h>
#import <Accelerate/Accelerate.h>

@implementation DrawView

@synthesize mgr = _mgr;
@synthesize drawTimer = _drawTimer;

-(id)initWithCoder:(NSCoder *)coder{
    self = [super initWithCoder:coder];
    if(self){
        _drawTimer = [NSTimer scheduledTimerWithTimeInterval:1./30. target:self selector:@selector(drawRect:) userInfo:nil repeats:YES];
        _leftBuffer = NULL;
        _rightBuffer = NULL;
    }
    return self;
}

-(void)setMgr:(BufferManager *)mgr{
    _mgr = mgr;
    if(_leftBuffer != NULL)free(_leftBuffer);
    if(_rightBuffer != NULL)free(_rightBuffer);
    _leftBuffer = (GLfloat*)calloc(2 * _mgr.drawLen, sizeof(GLfloat));
    _rightBuffer = (GLfloat*)calloc(2 * _mgr.drawLen, sizeof(GLfloat));
    for(UInt32 i = 0 ; i < _mgr.drawLen ; i ++){
        _leftBuffer[2*i] = _rightBuffer[2*i] = -1. + 2. * i / _mgr.drawLen; // x
        _leftBuffer[2*i+1] = _rightBuffer[2*i+1] = 0.; // y
    }
}

-(void)dealloc{
    [_drawTimer invalidate];
    if(_leftBuffer != NULL)free(_leftBuffer);
    if(_rightBuffer != NULL)free(_rightBuffer);
}

-(void)prepareBuffer{
    float lM = 1,lm = 0,rM = 1,rm = 0;
    vDSP_minv(_mgr.leftDrawBuffer, 1, &lm, _mgr.drawLen);
    vDSP_maxv(_mgr.leftDrawBuffer, 1, &lM, _mgr.drawLen);
    vDSP_minv(_mgr.rightDrawBuffer, 1, &rm, _mgr.drawLen);
    vDSP_maxv(_mgr.rightDrawBuffer, 1, &rM, _mgr.drawLen);
    for(UInt32 i = 0 ; i < _mgr.drawLen ; i ++){
        _leftBuffer[2 * i + 1] = 0.5 + 0.5 * (_mgr.leftDrawBuffer[i] - lm ) / (lM - lm);
        _rightBuffer[2 * i + 1] = -0.5 + 0.5 * (_mgr.rightDrawBuffer[i] - rm) / (rM - rm);
    }
}

-(void) drawRect: (NSRect) bounds
{
    if(_mgr == nil)return;
    [self prepareBuffer];
    glLineWidth(1.);
    glViewport(0,0,(float)self.frame.size.width,(float)self.frame.size.height);
    
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    glEnableClientState(GL_VERTEX_ARRAY);
    
    glColor4f(0., 1., 0., 1.);
    glVertexPointer(2, GL_FLOAT, 0, _leftBuffer);
    glDrawArrays(GL_LINE_STRIP, 0, _mgr.drawLen);
    
    glColor4f(1., 0., 0., 1.);
    glVertexPointer(2, GL_FLOAT, 0, _rightBuffer);
    glDrawArrays(GL_LINE_STRIP, 0, _mgr.drawLen);
    
    glDisableClientState(GL_VERTEX_ARRAY);
    glFlush();
}

@end
