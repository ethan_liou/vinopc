//
//  ViewController.h
//  VinoTwo
//
//  Created by Ethan on 2014/11/12.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DrawView.h"
#import "BufferManager.h"

@interface ViewController : NSViewController<BufferDelegate>{
    IBOutlet NSButton * _startBtn;
    IBOutlet NSTextField * _distanceTF;
    IBOutlet NSTextField * _resLbl;
    IBOutlet DrawView * _glView;
    BOOL _isstart;
}

@property (nonatomic, strong) BufferManager * mgr;

@end

