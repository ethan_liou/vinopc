//
//  AppDelegate.h
//  VinoThree
//
//  Created by Ethan on 2014/11/16.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ViewController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (nonatomic, strong) ViewController * controller;

@end

