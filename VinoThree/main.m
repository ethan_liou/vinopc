//
//  main.m
//  VinoThree
//
//  Created by Ethan on 2014/11/16.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
