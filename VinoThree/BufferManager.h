//
//  BufferManager.h
//  VinoTwo
//
//  Created by Ethan on 2014/11/15.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FFTHelper.h"
#import <vector>

@protocol BufferDelegate <NSObject>

-(void)updateProgress:(int)progress AndTotal:(int)total;
-(void)updateWithMean:(float)mean AndStdev:(float)stdev AndHeartRate:(float)rate AndHeartStdev:(float)rateStdev;

@end

@interface BufferManager : NSObject{
    float * _leftDrawBuffer;
    float * _rightDrawBuffer;
    int _drawLen;
    float * _rawBuffer[2];
    float * _fftBuffer[2];
    int _shiftIdx;
    int _shiftLen;
    std::vector<float> _avgVec;
    std::vector<float> _heartVec;
    FFTHelper * _fftHelper;
}

@property (readonly) float * leftDrawBuffer;
@property (readonly) float * rightDrawBuffer;
@property (readonly) int drawLen;
@property (nonatomic, weak) id<BufferDelegate> delegate;

-(id)initWithDrawLength:(int)len;
-(void)InsertAudioData:(float*)data WithNumSample:(UInt32)len;
-(void)clearData;

@end
