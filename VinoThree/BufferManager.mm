//
//  BufferManager.m
//  VinoTwo
//
//  Created by Ethan on 2014/11/15.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import "BufferManager.h"
#import <sys/time.h>
#import <Accelerate/Accelerate.h>

@implementation BufferManager

@synthesize drawLen = _drawLen;
@synthesize leftDrawBuffer = _leftDrawBuffer;
@synthesize rightDrawBuffer = _rightDrawBuffer;
@synthesize delegate = _delegate;

#define STDEV_SZ 10

-(id)initWithDrawLength:(int)len{
    self = [super init];
    NSLog(@"Init Buffer");
    if (self){
        _drawLen = len;
        _leftDrawBuffer = (float*)calloc(len, sizeof(float));
        _rightDrawBuffer = (float*)calloc(len, sizeof(float));
        _shiftLen = len * 8;
        _rawBuffer[0] = (float*)calloc(_shiftLen, sizeof(float));
        _rawBuffer[1] = (float*)calloc(_shiftLen, sizeof(float));
        _fftBuffer[0] = (float*)calloc(_shiftLen, sizeof(float));
        _fftBuffer[1] = (float*)calloc(_shiftLen, sizeof(float));
        _shiftIdx = 0;
        _fftHelper = new FFTHelper(_shiftLen);
        _avgVec.reserve(STDEV_SZ);
        _heartVec.reserve(STDEV_SZ);
    }
    return self;
}

-(void)dealloc{
    NSLog(@"Dealloc Buffer");
    free(_leftDrawBuffer);
    free(_rightDrawBuffer);
    free(_rawBuffer[0]);
    free(_rawBuffer[1]);
    free(_fftBuffer[0]);
    free(_fftBuffer[1]);
    delete _fftHelper;
}

void lowpass(DSPSplitComplex* freq, UInt32 len, Float32 freqInterval){
    for(UInt32 i = 0 ; i < len ; i ++){
        if(i > 50){
            freq->realp[i] = 0;
            freq->imagp[i] = 0;
        }
    }
}

float subMeanVec[STDEV_SZ];
float squared[STDEV_SZ];
- (void)stdevWithPtr:(float*)ptr Length:(UInt32)length OutputMean:(float&)mean OutputStdev:(float&)stdev {
    vDSP_meanv(ptr, 1, &mean, length); // calc mean
    mean = -1 * mean;
    vDSP_vsadd(ptr,1,&mean,subMeanVec,1,length); // x - mean
    vDSP_vsq(subMeanVec,1,squared,1,length); // (x - mean)^2
    Float32 sum = 0;
    vDSP_sve(squared,1,&sum,length); // sum ( (x-mean) ^ 2 )
    stdev = sqrt( fabsf(sum/length-1) ); // sqrt ( sum / length )
    mean = -1 * mean;
}

-(void)doShift{
    memcpy(_fftBuffer[0], _rawBuffer[0], _shiftLen * sizeof(float));
    memcpy(_fftBuffer[1], _rawBuffer[1], _shiftLen * sizeof(float));
    // fft
    for(int i = 0 ; i < 2 ; i ++){
        _fftHelper->Process(_fftBuffer[i], _fftBuffer[i], lowpass);
    }
    
    // normalize
    unsigned long minIdx[2], maxIdx[2];
    for(int i = 0 ; i < 2 ; i ++){
        float m,M;
        vDSP_maxvi(_fftBuffer[i], 1, &M, &maxIdx[i], _shiftLen);
        vDSP_minvi(_fftBuffer[i], 1, &m, &minIdx[i], _shiftLen);
        float B = 1. / (M - m);
        float C = -m / (M - m);
        vDSP_vsmsa(_fftBuffer[i], 1, &B, &C, _fftBuffer[i], 1, _shiftLen);
    }

    // find 0.6 and 0.4 avg x
    float avg[2];
    std::vector<float> vec(100);
    for(int i = 0 ; i < 2 ; i ++){
        vec.clear();
        for (unsigned long j = minIdx[i]; j > 0; j--) {
            if(_fftBuffer[i][j] > 0.55)
                break;
            if(_fftBuffer[i][j] > 0.45)
                vec.push_back(j);
        }
        if(vec.size() == 0){
            return;
        }
        vDSP_meanv(vec.data(), 1, &avg[i], vec.size());
    }
    
    // heart
    static timeval prevHeart = {0};
    if(prevHeart.tv_sec == 0){
        gettimeofday(&prevHeart, 0);
    } else {
        timeval curr;
        gettimeofday(&curr, 0);
        if(_heartVec.size() == STDEV_SZ){
            _heartVec.erase(_heartVec.begin());
        }
        _heartVec.push_back( curr.tv_sec * 1000 + curr.tv_usec / 1000 - prevHeart.tv_sec * 1000 - prevHeart.tv_usec / 1000 );
        prevHeart.tv_usec = curr.tv_usec;
        prevHeart.tv_sec = curr.tv_sec;
    }
    
    // vino
    if(_avgVec.size() == STDEV_SZ){
        _avgVec.erase(_avgVec.begin());
    }
    _avgVec.push_back(avg[1] - avg[0]);

    NSLog(@"%d %d",_avgVec.size(), _heartVec.size());
    if(_avgVec.size() == STDEV_SZ && _heartVec.size() == STDEV_SZ){
        float heartMean, heartStdev, vinoMean, vinoStdev;
        [self stdevWithPtr:_avgVec.data() Length:_avgVec.size() OutputMean:vinoMean OutputStdev:vinoStdev];
        [self stdevWithPtr:_heartVec.data() Length:_heartVec.size() OutputMean:heartMean OutputStdev:heartStdev];
        if(_delegate){
            [_delegate updateWithMean:vinoMean AndStdev:vinoStdev AndHeartRate:heartMean AndHeartStdev:heartStdev];
        }
    } else {
        if (_delegate){
            [_delegate updateProgress:_avgVec.size() AndTotal:STDEV_SZ];
        }
    }

}

-(void)clearData{
    _heartVec.clear();
    _avgVec.clear();
}

-(void)InsertAudioData:(float*)data WithNumSample:(UInt32)len{
    // raw data
    if(_shiftIdx == _shiftLen){
        for(int idx = 0 ; idx < 2 ; idx++){
            memmove(_rawBuffer[idx], &_rawBuffer[idx][len], (_shiftLen - len) * sizeof(float));

            for(UInt32 i = 0; i < len ; i ++){
                _rawBuffer[idx][_shiftLen - len + i] = data[2 * i + idx];
            }
        }
        
        // right min
        float M;
        unsigned long minIdx;
        vDSP_minvi(_rawBuffer[1], 1, &M, &minIdx, _shiftLen);
        if( minIdx > _shiftLen / 2 - len / 2 && minIdx < _shiftLen / 2 + len / 2 ){
            [self doShift];
        }
    } else {
        for(UInt32 i = 0; i < len ; i ++){
            for(int idx = 0 ; idx < 2 ; idx++){
                _rawBuffer[idx][_shiftIdx] = data[2*i];
            }
            _shiftIdx++;
        }
    }
    
#define SLOW 16
    len /= SLOW;
    // left
    memmove(_leftDrawBuffer, &_leftDrawBuffer[len], (_drawLen - len)*sizeof(float));
    for(UInt32 i = 0 ; i < len ; i ++){
        _leftDrawBuffer[_drawLen - len + i] = data[2 * i * SLOW];
    }
    // right
    memmove(_rightDrawBuffer, &_rightDrawBuffer[len], (_drawLen - len)*sizeof(float));
    for(UInt32 i = 0 ; i < len ; i ++){
        _rightDrawBuffer[_drawLen - len + i] = data[2 * i * SLOW + 1];
    }
}

@end
