//
//  ViewController.m
//  VinoTwo
//
//  Created by Ethan on 2014/11/12.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import "ViewController.h"
#import "Novocaine.h"

@implementation ViewController

@synthesize mgr = _mgr;

#define DRAW_LEN 4096

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        _mgr = [[BufferManager alloc] initWithDrawLength:DRAW_LEN];
        _mgr.delegate = self;
    }
    return self;

}

- (IBAction)startOrStop:(id)sender{
    if(_distanceTF.floatValue == 0){
        _resLbl.stringValue = [NSString stringWithFormat:@"Invalid distance %@",_distanceTF.stringValue];
        return;
    }
    _isstart = !_isstart;
    Novocaine *audioManager = [Novocaine audioManager];
    if (_isstart){
        _resLbl.stringValue = @"";
        [_mgr clearData];
        [audioManager play];
    } else {
        [audioManager pause];
    }
    _startBtn.title = _isstart ? @"Stop" : @"Start";
    _distanceTF.enabled = !_isstart;
}

- (void)loadView{
    [super loadView];
    _glView.mgr = _mgr;
    // Do any additional setup after loading the view.
    Novocaine *audioManager = [Novocaine audioManager];
    [audioManager setInputBlock:^(float *newAudio, UInt32 numSamples, UInt32 numChannels) {
        [_mgr InsertAudioData:newAudio WithNumSample:numSamples];
    }];
    _isstart = NO;
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

-(void)updateProgress:(int)progress AndTotal:(int)total{
    _resLbl.stringValue = [NSString stringWithFormat:@"Preparing %.0f%%",100. * progress/total];
}

-(void)updateWithMean:(float)mean AndStdev:(float)stdev AndHeartRate:(float)rate AndHeartStdev:(float)rateStdev{
    // mean / 44.1 ms
    // x / t = v
    float meanVelocity = _distanceTF.floatValue * 1000 * 44.1 / mean;
    float stdevVelocity = meanVelocity * stdev / mean;

    float heartBps = 60000 / rate;
    float heartStdev = heartBps * rateStdev / rate;

    _resLbl.stringValue = [NSString stringWithFormat:@"%.01f ± %.01f cm/s (%.01f%%), %.01f ms, Heart:%.0f ± %.1f bpm", meanVelocity, stdevVelocity, 100 * stdev / mean, mean / 44.1, heartBps, heartStdev];
}

@end
