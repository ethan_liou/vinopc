//
//  AppDelegate.m
//  VinoThree
//
//  Created by Ethan on 2014/11/16.
//  Copyright (c) 2014年 VinoTechs. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    _controller = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    _window.contentView = _controller.view;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
